gem_package "sass" do
  version "3.1.12"
end

gem_package "sprockets" do
  version "1.0.2"
end

gem_package "rake" do
  version "0.9.2"
end

# Optional but prettifies output
gem_package "colored"
gem_package "capistrano"
gem_package "railsless-deploy"

site = {
  :name => "tnf", 
  :host => "tnf", 
  :aliases => ["local.tnf.fluid.com"]
}

execute "disable-default-site" do
  command "sudo a2dissite default"
end

# Apache needs to know the path to make the CGI script function correctly
execute "add path" do
  command "echo 'export PATH=/opt/vagrant_ruby/bin:$PATH' >> /etc/apache2/envvars"
  user "root"
  group "root"
end

# 
execute "add logs" do
  command "echo \"alias tnf-error='tail -f  /var/log/apache2/tnf-error.log'\nalias tnf-access='tail -f  /var/log/apache2/tnf-access.log'\" >> /home/vagrant/.bash_aliases"
  user "root"
  group "root"
end

# Set up hostname
execute "add host" do
  command "echo '127.0.0.1 local.tnf.fluid.com' >> /etc/hosts"
  user "root"
  group "root"
end

web_app site[:name] do
  template "tnf.conf.erb"
  server_name site[:host]
  server_aliases site[:aliases]
  docroot "/tnf/htdocs"
  notifies :restart, resources(:services => "apache2")
end
